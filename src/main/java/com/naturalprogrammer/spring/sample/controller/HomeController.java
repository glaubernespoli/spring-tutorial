package com.naturalprogrammer.spring.sample.controller;

import javax.validation.Valid;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.naturalprogrammer.spring.sample.dto.ForgotPasswordForm;
import com.naturalprogrammer.spring.sample.dto.ResetPasswordForm;
import com.naturalprogrammer.spring.sample.dto.SignupForm;
import com.naturalprogrammer.spring.sample.service.UserService;
import com.naturalprogrammer.spring.sample.util.MyUtil;
import com.naturalprogrammer.spring.sample.validator.ForgotPasswordFormValidator;
import com.naturalprogrammer.spring.sample.validator.ResetPasswordFormValidator;
import com.naturalprogrammer.spring.sample.validator.SignupFormValidator;

/**
 * @author glaubernespoli
 * @created dez 09, 2015
 */
@Controller
public class HomeController {

	private static final Log log = LogFactory.getLog(HomeController.class);

	private UserService userService;

	private SignupFormValidator signupFormValidator;

	private ForgotPasswordFormValidator forgotPasswordFormValidator;

	private ResetPasswordFormValidator resetPasswordFormValidator;

	@Autowired
	public HomeController(UserService userService, SignupFormValidator signupFormValidator, ForgotPasswordFormValidator forgotPasswordFormValidator, ResetPasswordFormValidator resetPasswordFormValidator) {
		this.userService = userService;
		this.signupFormValidator = signupFormValidator;
		this.forgotPasswordFormValidator = forgotPasswordFormValidator;
		this.resetPasswordFormValidator = resetPasswordFormValidator;
	}

	@InitBinder("signUp")
	protected void initSignUpBinder(WebDataBinder webDataBinder) {
		webDataBinder.setValidator(signupFormValidator);
	}

	@InitBinder("forgotPasswordForm")
	protected void initForgotPasswordBinder(WebDataBinder webDataBinder) {
		webDataBinder.setValidator(forgotPasswordFormValidator);
	}

	@InitBinder("resetPasswordForm")
	protected void initResetPasswordBinder(WebDataBinder webDataBinder) {
		webDataBinder.setValidator(resetPasswordFormValidator);
	}

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model) {
		model.addAttribute("signUp", new SignupForm());
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signup(@ModelAttribute("signUp") @Valid SignupForm signupForm, BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			return "signup";
		}

		userService.signup(signupForm);
		log.info(signupForm.toString());

		MyUtil.flash(redirectAttributes, "success", "signup.success");
		return UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/";
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.GET)
	public String forgotPassword(Model model) {
		model.addAttribute("forgotPasswordForm", new ForgotPasswordForm());
		return "forgot-password";
	}

	@RequestMapping(value = "/forgot-password", method = RequestMethod.POST)
	public String forgotPassword(@ModelAttribute("forgotPasswordForm") @Valid ForgotPasswordForm forgotPasswordForm, BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			return "forgot-password";
		}

		userService.forgotPassword(forgotPasswordForm);
//		log.info(signupForm.toString());

		MyUtil.flash(redirectAttributes, "info", "checkMailResetPassword");
		return UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/";
	}

	@RequestMapping(value = "/reset-password/{forgotPasswordCode}", method = RequestMethod.GET)
	public String resetPassword(@PathVariable("forgotPasswordCode") String forgotPasswordCode, Model model) {
		model.addAttribute("resetPasswordForm", new ResetPasswordForm());
				return "reset-password";
	}

	@RequestMapping(value = "/reset-password/{forgotPasswordCode}", method = RequestMethod.POST)
	public String resetPassword(@PathVariable("forgotPasswordCode") String forgotPasswordCode, @ModelAttribute("resetPasswordForm") @Valid ResetPasswordForm resetPasswordForm,
	                            BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			return "reset-password";
		}

		userService.resetPassword(forgotPasswordCode, resetPasswordForm, result);

		if(result.hasErrors()) {
			return "reset-password";
		}

		MyUtil.flash(redirectAttributes, "info", "passwordChanged");
		return UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/login";
	}
}

package com.naturalprogrammer.spring.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableAsync
public class Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(Application.class, args);

		//		LOGGER.info("Beans in Application Context: ");
		//
		//		String beans[] = applicationContext.getBeanDefinitionNames();
		//		Arrays.sort(beans);
		//
		//		for (String bean : beans) {
		//			LOGGER.info(bean);
		//		}
	}

//	@Bean(name = "localeResolver")
//	public LocaleResolver localeResolver() {
//		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
//		localeResolver.setDefaultLocale(new Locale("pt", "BR"));
//		return localeResolver;
//	}
//
//	@Bean
//	public LocaleChangeInterceptor localeChangeInterceptor() {
//		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
//		localeChangeInterceptor.setParamName("language");
//		return localeChangeInterceptor;
//	}
//
//	@Bean
//	public ControllerClassNameHandlerMapping controllerClassNameHandlerMapping() {
//		ControllerClassNameHandlerMapping controllerClassNameHandlerMapping = new ControllerClassNameHandlerMapping();
//		controllerClassNameHandlerMapping.setInterceptors(new Object[]{ localeChangeInterceptor() });
//		return controllerClassNameHandlerMapping;
//	}
}

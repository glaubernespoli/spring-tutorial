package com.naturalprogrammer.spring.sample.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 18, 2015
 */
public class ResetPasswordForm {

	@NotBlank
	@Size(min = 6, max = User.PASSWORD_MAX, message = "{signup.password.size}")
	private String password;

	@NotBlank
	@Size(min = 6, max = User.PASSWORD_MAX, message = "{signup.password.size}")
	private String retypePassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRetypePassword() {
		return retypePassword;
	}

	public void setRetypePassword(String retypePassword) {
		this.retypePassword = retypePassword;
	}
}

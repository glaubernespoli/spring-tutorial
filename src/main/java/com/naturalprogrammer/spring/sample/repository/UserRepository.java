package com.naturalprogrammer.spring.sample.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 15, 2015
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

	User findByForgotPasswordCode(String forgotPasswordCode);
}

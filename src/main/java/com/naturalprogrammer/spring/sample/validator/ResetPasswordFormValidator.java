package com.naturalprogrammer.spring.sample.validator;

import javax.validation.ParameterNameProvider;
import javax.validation.executable.ExecutableValidator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.naturalprogrammer.spring.sample.dto.ResetPasswordForm;

/**
 * @author glaubernespoli
 * @created dez 18, 2015
 */
@Component
public class ResetPasswordFormValidator extends LocalValidatorFactoryBean {

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(ResetPasswordForm.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {
		super.validate(target, errors, validationHints);

		if(!errors.hasErrors()) {
			ResetPasswordForm resetPasswordForm = (ResetPasswordForm) target;
			if(!resetPasswordForm.getPassword().equals(resetPasswordForm.getRetypePassword())) {
				errors.reject("passwordDoNotMatch");
			}
		}
	}

	@Override
	public ExecutableValidator forExecutables() {
		return null;
	}

	@Override
	public ParameterNameProvider getParameterNameProvider() {
		return null;
	}
}

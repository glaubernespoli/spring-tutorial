package com.naturalprogrammer.spring.sample.dto;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 18, 2015
 */
public class ForgotPasswordForm {

	@NotBlank
	@Size(min = 1, max = User.EMAIL_MAX, message = "{signup.email.size}")
	@Email(message = "{signup.email.invalid}")
	private String email;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}

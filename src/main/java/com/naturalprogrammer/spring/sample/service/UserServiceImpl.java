package com.naturalprogrammer.spring.sample.service;

import java.util.Objects;

import javax.mail.MessagingException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.validation.BindingResult;

import com.naturalprogrammer.spring.sample.dto.ForgotPasswordForm;
import com.naturalprogrammer.spring.sample.dto.ResetPasswordForm;
import com.naturalprogrammer.spring.sample.dto.SignupForm;
import com.naturalprogrammer.spring.sample.dto.UserDetailsImpl;
import com.naturalprogrammer.spring.sample.dto.UserEditForm;
import com.naturalprogrammer.spring.sample.entity.User;
import com.naturalprogrammer.spring.sample.mail.MailSender;
import com.naturalprogrammer.spring.sample.repository.UserRepository;
import com.naturalprogrammer.spring.sample.util.MyUtil;

/**
 * @author glaubernespoli
 * @created dez 15, 2015
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class UserServiceImpl implements UserService, UserDetailsService {

	private static final Log logger = LogFactory.getLog(UserServiceImpl.class);

	private final UserRepository userRepository;

	private PasswordEncoder passwordEncoder;

	private MailSender mailSender;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, MailSender mailSender) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.mailSender = mailSender;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void signup(SignupForm signupForm) {
		User user = new User();
		user.setEmail(signupForm.getEmail());
		user.setName(signupForm.getName());
		user.getRoles().add(User.Role.UNVERIFIED);
		user.setVerificationCode(RandomStringUtils.randomAlphanumeric(User.RANDOM_CODE_LENGTH));
		user.setPassword(passwordEncoder.encode(signupForm.getPassword()));
		userRepository.save(user);

		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCommit() {
				String verifyLink = MyUtil.getSiteUrl() + "/users/" + user.getVerificationCode() + "/verify";
				try {
					mailSender.send(user.getEmail(), MyUtil.getMessage("email.verify.subject"), MyUtil.getMessage("email.verify.body", verifyLink));
					logger.info("Verification mail to " + user.getEmail() + " queued.");
				} catch (MessagingException e) {
					logger.error(ExceptionUtils.getStackTrace(e));
				}
			}
		});

	}

	@Override
	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public void verify(String verificationCode) {
		Long loggedInUserId = MyUtil.getSessionUser().getId();
		User user = userRepository.findOne(loggedInUserId);

		MyUtil.validate(user.getRoles().contains(User.Role.UNVERIFIED), "user.already.verified");
		MyUtil.validate(user.getVerificationCode().equals(verificationCode), "incorrect", "verification code");

		user.getRoles().remove(User.Role.UNVERIFIED);
		user.setVerificationCode(null);
		userRepository.save(user);
	}

	@Override
	public void forgotPassword(ForgotPasswordForm forgotPasswordForm) {
		final User user = userRepository.findByEmail(forgotPasswordForm.getEmail());
		final String forgotPasswordCode = RandomStringUtils.randomAlphanumeric(User.RANDOM_CODE_LENGTH);

		user.setForgotPasswordCode(forgotPasswordCode);
		final User savedUser = userRepository.save(user);

		TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
			@Override
			public void afterCommit() {
				try {
					mailForgotPasswordLink(savedUser);
				} catch (MessagingException e) {
					logger.error(ExceptionUtils.getStackTrace(e));
				}
			}
		});
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void resetPassword(String forgotPasswordCode, ResetPasswordForm resetPasswordForm, BindingResult result) {
		User user = userRepository.findByForgotPasswordCode(forgotPasswordCode);
		if(user == null) {
			result.reject("invalidForgotPasswordCode");
		}

		if(result.hasErrors()) {
			return;
		}

		user.setForgotPasswordCode(null);
		user.setPassword(passwordEncoder.encode(resetPasswordForm.getPassword().trim()));
		userRepository.save(user);
	}

	@Override
	public User findOne(Long userId) {
		User loggedIn = MyUtil.getSessionUser();
		User user = userRepository.findOne(userId);
		if(loggedIn == null || (!Objects.equals(loggedIn.getId(), user.getId()) && !loggedIn.isAdmin())) {
			user.setEmail("Confidential");
		}
		return user;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(Long userId, UserEditForm userEditForm) {
		User loggedIn = MyUtil.getSessionUser();
		MyUtil.validate(loggedIn.isAdmin() || Objects.equals(loggedIn.getId(), userId), "noPermission");

		User user = userRepository.findOne(userId);
		user.setName(userEditForm.getName());

		if(loggedIn.isAdmin()) {
			user.setRoles(userEditForm.getRoles());
		}
		userRepository.save(user);
	}

	private void mailForgotPasswordLink(User user) throws MessagingException {
		String forgotPasswordLink = MyUtil.getSiteUrl() + "/reset-password/" + user.getForgotPasswordCode();
		mailSender.send(user.getEmail(), MyUtil.getMessage("forgotPasswordSubject"), MyUtil.getMessage("forgotPasswordBody", forgotPasswordLink));
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepository.findByEmail(email);

		if(user == null) {
			throw new UsernameNotFoundException("User not found for email " + email + ".");
		}
		return new UserDetailsImpl(user);
	}
}

<%@include file="includes/header.jsp"%>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Profile</h3>
	</div>

	<div class="panel-body">
		<dl class="dl-horizontal">
			<dt>Name</dt>
			<dd><c:out value="${user.name}" /></dd>
			<dt>Email</dt>
			<dd><c:out value="${user.email}" /></dd>
			<dt>Roles</dt>
			<dd><c:out value="${user.roles}" /></dd>
		</dl>
	</div>

	<c:if test="${user.editable}">
		<div class="panel-footer">
			<a href="/users/${user.id}/edit" class="btn btn-link">Edit</a>
			<a href="/users/${user.id}/change-password" class="btn btn-link">Change password</a>
			<a href="/users/${user.id}/change-email" class="btn btn-link">Change email</a>
		</div>
	</c:if>
</div>
<%@include file="includes/footer.jsp"%>
package com.naturalprogrammer.spring.sample.dto;

import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 14, 2015
 */
public class SignupForm {

	@NotBlank(message = "{signup.email.blank}")
	@Size(min = 1, max = 250, message = "{signup.email.size}")
	@Email(message = "{signup.email.invalid}")
//	@Pattern(regexp = "[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
	private String email;

	@NotBlank(message = "{signup.name.blank}")
	@Size(min = 1, max = 100, message = "{signup.name.size}")
	private String name;

	@NotBlank(message = "{signup.password.blank}")
	@Size(min = 6, max = User.PASSWORD_MAX, message = "{signup.password.size}")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}

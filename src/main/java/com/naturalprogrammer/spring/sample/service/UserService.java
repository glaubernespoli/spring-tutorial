package com.naturalprogrammer.spring.sample.service;

import org.springframework.validation.BindingResult;

import com.naturalprogrammer.spring.sample.dto.ForgotPasswordForm;
import com.naturalprogrammer.spring.sample.dto.ResetPasswordForm;
import com.naturalprogrammer.spring.sample.dto.SignupForm;
import com.naturalprogrammer.spring.sample.dto.UserEditForm;
import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 15, 2015
 */
public interface UserService {

	void signup(SignupForm signupForm);

	User findByEmail(String email);

	void verify(String verificationCode);

	void forgotPassword(ForgotPasswordForm forgotPasswordForm);

	void resetPassword(String forgotPasswordCode, ResetPasswordForm resetPasswordForm, BindingResult result);

	User findOne(Long userId);

	void update(Long userId, UserEditForm userEditForm);
}

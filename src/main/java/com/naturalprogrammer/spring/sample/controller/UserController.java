package com.naturalprogrammer.spring.sample.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.naturalprogrammer.spring.sample.dto.UserEditForm;
import com.naturalprogrammer.spring.sample.entity.User;
import com.naturalprogrammer.spring.sample.service.UserService;
import com.naturalprogrammer.spring.sample.util.MyUtil;

/**
 * @author glaubernespoli
 * @created dez 17, 2015
 */
@Controller
@RequestMapping("/users")
public class UserController {

	private UserService userService;

	@Autowired
	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	@RequestMapping("/{verificationCode}/verify")
	public String verify(HttpServletRequest request, @PathVariable("verificationCode") String verificationCode, RedirectAttributes redirectAttributes) throws ServletException {
		userService.verify(verificationCode);
		MyUtil.flash(redirectAttributes, "success", "user.verification.success");
		request.logout();

		return UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/";
	}

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public String profile(@PathVariable("userId") Long userId, Model model) {
		model.addAttribute("user", userService.findOne(userId));
		return "profile";
	}

	@RequestMapping(value = "/{userId}/edit", method = RequestMethod.GET)
	public String editProfile(@PathVariable("userId") Long userId, Model model) {
		User user = userService.findOne(userId);
		UserEditForm form = new UserEditForm();
		form.setName(user.getName());
		form.setRoles(user.getRoles());
		model.addAttribute(form);

		return "user-edit";
	}

	@RequestMapping(value = "/{userId}/edit", method = RequestMethod.POST)
	public String editProfile(HttpServletRequest request, @PathVariable("userId") Long userId, @ModelAttribute("userEditForm") @Valid UserEditForm userEditForm, BindingResult
			result, RedirectAttributes redirectAttributes) throws ServletException {
		if(result.hasErrors()) {
			return "user-edit";
		}

		userService.update(userId, userEditForm);
		MyUtil.flash(redirectAttributes, "success", "editSuccessful");
		request.logout();

		return UrlBasedViewResolver.REDIRECT_URL_PREFIX + "/";
	}
}

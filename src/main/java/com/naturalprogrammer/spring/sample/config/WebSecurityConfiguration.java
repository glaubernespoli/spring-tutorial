package com.naturalprogrammer.spring.sample.config;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;

/**
 * @author glaubernespoli
 * @created dez 16, 2015
 */
@Configuration
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final Log logger = LogFactory.getLog(WebSecurityConfiguration.class);

	@Resource
	private UserDetailsService userService;

	@Value("rememberMe.privateKey")
	private String rememberMeKey;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.authorizeRequests()
				.antMatchers("/",
						"/home",
						"/error",
						"/signup",
						"/forgot-password",
						"/reset-password/*",
						"/public/**",
						"/users/*").permitAll()
				.anyRequest().authenticated();
		http
				.formLogin()
					.loginPage("/login")
					.permitAll()
				.and()
				.rememberMe()
					.key(rememberMeKey)
					.rememberMeServices(rememberMeServices())
				.and()
				.logout()
					.permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userService).passwordEncoder(passwordEncoder());
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		logger.info("Creating password encoder bean");
		return new BCryptPasswordEncoder();
	}

	@Bean
	public RememberMeServices rememberMeServices() {
		return new TokenBasedRememberMeServices(rememberMeKey, userService);
	}
}

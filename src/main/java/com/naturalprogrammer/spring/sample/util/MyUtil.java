package com.naturalprogrammer.spring.sample.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.naturalprogrammer.spring.sample.dto.UserDetailsImpl;
import com.naturalprogrammer.spring.sample.entity.User;

/**
 * @author glaubernespoli
 * @created dez 15, 2015
 */
@Component
public final class MyUtil {

	private static MessageSource messageSource;

	private static String siteUrl;

	@Autowired
	private MyUtil(MessageSource messageSource, @Value("${site.url}") String siteUrl) {
		MyUtil.messageSource = messageSource;
		MyUtil.siteUrl = siteUrl;
	}

	public static void flash(RedirectAttributes redirectAttributes, String kind, String messageKey) {
		redirectAttributes.addFlashAttribute("flashKind", kind);
		redirectAttributes.addFlashAttribute("flashMessage", getMessage(messageKey));
	}

	public static String getSiteUrl() {
		return siteUrl;
	}

	public static void setSiteUrl(String siteUrl) {
		MyUtil.siteUrl = siteUrl;
	}

	public static String getMessage(String messageKey, Object... args) {
		return messageSource.getMessage(messageKey, args, LocaleContextHolder.getLocale());
	}

	public static User getSessionUser() {
		UserDetailsImpl auth = getAuth();
		return auth == null ? null : auth.getUser();
	}

	public static UserDetailsImpl getAuth() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if(auth != null) {
			Object principal = auth.getPrincipal();
			if(UserDetailsImpl.class.isAssignableFrom(principal.getClass())) {
				return (UserDetailsImpl) principal;
			}
		}
		return null;
	}

	public static void validate(boolean valid, String errorMessageKey, Object... args) {
		if (!valid) {
			throw new RuntimeException(getMessage(errorMessageKey, args));
		}
	}
}

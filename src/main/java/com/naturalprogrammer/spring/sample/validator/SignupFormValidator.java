package com.naturalprogrammer.spring.sample.validator;

import javax.validation.ParameterNameProvider;
import javax.validation.executable.ExecutableValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.naturalprogrammer.spring.sample.dto.SignupForm;
import com.naturalprogrammer.spring.sample.entity.User;
import com.naturalprogrammer.spring.sample.service.UserService;

/**
 * @author glaubernespoli
 * @created dez 15, 2015
 */
@Component
public class SignupFormValidator extends LocalValidatorFactoryBean {

	private UserService userService;

	@Autowired
	public SignupFormValidator(UserService userService) {
		this.userService = userService;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return clazz.isAssignableFrom(SignupForm.class);
	}

	@Override
	public void validate(Object target, Errors errors, Object... validationHints) {
		super.validate(target, errors, validationHints);

		if(!errors.hasErrors()) {
			SignupForm signupForm = (SignupForm) target;
			User user = userService.findByEmail(signupForm.getEmail());
			if(user != null) {
				errors.rejectValue("email", "signup.email.not.unique");
			}
		}
	}

	@Override
	public ExecutableValidator forExecutables() {
		return null;
	}

	@Override
	public ParameterNameProvider getParameterNameProvider() {
		return null;
	}
}
